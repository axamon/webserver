# webserver

webserver in Golang utile per uso locale.

Se volete la versione https e http2 richiedetela con una issue.

# Esempio

Ogni file nella directory ./static sarà raggiungible via http

```GO
package main

import (
	"context"

	"gitlab.com/axamon/webserver"
)

func main() {
    ctx, cancel := context.WithCancel(context.BackGroudn())
    defer cancel()

    // Configura ip e porta TCP del webserver
    webserver.HTTPAddr = "127.0.0.1:8080"

   // Aggiunge rotte al webserver
   webserver.Router.HandleFunc("/ciao", ciao)

    // Avvia webserver
    webserver.Start(ctx)
}

func ciao(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Ciao!"))
}
```