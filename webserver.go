package webserver

import (
	"context"
	"net/http"

	"github.com/gorilla/mux"
)

var Router = mux.NewRouter() //Gorilla mux
var HTTPAddr = "0.0.0.0:8080"

func Start(ctx context.Context) {

	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	fs := http.FileServer(http.Dir("./static"))
	Router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", (fs))) //! GORILLA HANDLER

	go http.ListenAndServe(HTTPAddr, Router)
	go http.ListenAndServeTLS(":443", "go-server.crt", "go-server.key", fs)

	select {
	case <-ctx.Done():
		return
	}

}
